document.addEventListener('DOMContentLoaded', function () {
	const nav = document.querySelector('.navbar')
	const show = document.querySelector('#navbarNavAltMarkup')
	const links = document.querySelectorAll('.nav-link')

	function addShadow() {
		if (window.scrollY >= 300) {
			nav.classList.add('shadow-bg')
		} else {
			nav.classList.remove('shadow-bg')
		}
	}
	function removeNav() {
		show.classList.remove('show')
	}

	window.addEventListener('scroll', addShadow)
	window.addEventListener('click', removeNav)
})
